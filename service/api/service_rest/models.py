from django.db import models
from django.urls import reverse
# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=50, unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    # def __str__(self):
    #     return f"VIN number: {self.vin}"

class Appointment(models.Model):
    STATUS_CHOICE = (
        ('Pending', 'Pending'),
        ('Finished', 'Finished'),
        ('Canceled', 'Canceled'),
    )

    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200, null=True)
    reason = models.TextField()
    date_time = models.DateTimeField()
    status = models.CharField(max_length=25, choices=STATUS_CHOICE)

    technician = models.ForeignKey(
        Technician,
        related_name= "appointments",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.pk})


