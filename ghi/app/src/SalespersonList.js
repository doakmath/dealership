import { useState, useEffect } from 'react'

function SalespersonList() {
    const [salespeople, setSalespeople] = useState([])
    const fetchData = async () => {
        const url = "http://localhost:8090/api/salespeople/"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salespeople)
            }
        } catch (error) {
            console.error("Error retrieving salespeople: fetchData", error)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    async function deleteSalesperson(salespersonId) {
        const url = `http://localhost:8090/api/salespeople/${salespersonId}/`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("Error deleting salesperson: deleteSalesperson", error)
        }
    }


    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Salespeople</h1>
                <div>

                </div>
                <table className="table table-striped m-3">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID #</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                    return (
                        <tr key={ salesperson.id }>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                            <td>{ salesperson.employee_id }</td>
                            <td><button className="btn btn-outline-danger end" onClick={ () => deleteSalesperson(salesperson.id)} >Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
            </div>
        </div>
    )
}

export default SalespersonList
