import React, { useState, useEffect } from 'react';
import moment from 'moment';


function Appointments() {
    const [appointments, setAppointments] = useState([]);


    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/appointments/');
            const automobileResponse = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok && automobileResponse.ok) {
                const appointmentsData = await response.json();
                const automobileData = await automobileResponse.json();

                const pendingStatus = appointmentsData.appointments.filter(appointment => appointment.status ==='Pending')

                const vipVINs = new Set(automobileData.autos.map(auto => auto.vin));

                const vipAppointments = pendingStatus.map(appointment => {
                    const isVIP = vipVINs.has(appointment.vin);

                    const localDateTime = moment(appointment.date_time).local();

                    return {
                        id: appointment.id,
                        vin: appointment.vin,
                        customer: appointment.customer,
                        date: localDateTime.format("MMM Do YYYY"),
                        time: localDateTime.format("h:mm:ss A"),
                        technician: appointment.technician,
                        reason: appointment.reason,
                        status: appointment.status,
                        isVIP: isVIP
                    };
                });
                setAppointments(vipAppointments);

            } else {
                if (!response.ok) console.error('An error occurred fetching appointments');
                if (!automobileResponse.ok) console.error('An error occurred fetching automobileVO data');
            }
        } catch (error) {
            console.error('An error occurred:', error);
        }
    };

useEffect(() => {
    getData();
}, []);

    const handleAction = async (id, action) => {
        console.log("ID", id)
        const url = `http://localhost:8080/api/appointments/${id}/${action}/`;

        const response = await fetch(url, { method: 'PUT' });

        if (response.ok) {
            getData();
        } else {
            console.error(`Failed to ${action} the appointment`);
        }
    };

    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Service Appointments</h1>
                <table className="table table-striped m-3">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Is VIP?</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map((appointment) => (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.isVIP ? 'Yes' : 'No'}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                                <td>
                                    <button className="btn btn-danger" onClick={() => handleAction(appointment.id, 'cancel')}>Cancel</button>
                                    <button className="btn btn-success" onClick={() => handleAction(appointment.id, 'finish')}>Finish</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default Appointments;
