import React, {useState, useEffect} from 'react';

function VehicleModels() {
    const [vehicleModels, setVehicleModels] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setVehicleModels(data.models);
        } else {
            console.error('An error occurred fetching the data')
        }
    }
    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8100/api/models/${id}`
        await fetch(deleteUrl, {method: 'delete'})
        getData()
    }
    useEffect(() => {
        getData()
    }, []);

    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Vehicle Models</h1>
                <table className="table table-striped m-3">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {vehicleModels.map(vehicleModel => {
                            return (
                                <tr key={vehicleModel.id}>
                                    <td>{vehicleModel.name}</td>
                                    <td>{vehicleModel.manufacturer.name}</td>
                                    <td><img src={ vehicleModel.picture_url } alt="car" style={{ width: '100px', height:'100px'}} /></td>
                                    <td>
                                        <button className="btn btn-danger" onClick={() => {handleDelete(vehicleModel.id)}}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default VehicleModels;
