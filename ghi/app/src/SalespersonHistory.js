import { useState, useEffect } from 'react'

function SalesPersonHistory() {
    
    const [salesperson, setSalesPerson] = useState('')
    const [salespeople, setSalespeople] = useState([])
    const [sales, setSales] = useState([])

    const handleSelection = (event) => {
        const value = event.target.value
        setSalesPerson(value)
        fetchSalesData(value)
    }

    const fetchSalespeopleData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/'
        const salespeopleResponse = await fetch(salespeopleUrl)
        if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json()
            setSalespeople(salespeopleData.salespeople)
        }
    }

    const fetchSalesData = async (selection) => {
        const salesUrl = 'http://localhost:8090/api/sales/'
        const salesResponse = await fetch(salesUrl)
        if (salesResponse.ok) {
            const salesData = await salesResponse.json()
            const salesArray = salesData.sales
            const sales = salesArray.filter((sale) => (sale.salesperson.employee_id === selection))
            setSales(sales)
        }
    }

    useEffect(() => {
        fetchSalespeopleData()
    }, [])



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Salesperson History</h1>
                        <p>Choose a Salesperson to see their Sales History</p>
                        <div className="mb-3">
                            <select value={salesperson} onChange={handleSelection} required id="salespeople" name="salespeople" className="form-select">
                            <option value="">Salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}, ID: {salesperson.employee_id}</option>
                                )})}
                            </select>
                            <div className="alert alert-secondary" role="alert">
                                There are <h3>{sales.length}</h3> records.
                            </div>
                        </div>
                    </div>
                </div>
                    <div>
                        <table className="table table-striped m-3">
                            <thead>
                                <tr>
                                    <th>Automobile VIN</th>
                                    <th>Customer</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                {sales.map(sale => {
                                return (
                                    <tr key={ sale.id }>
                                        <td>{ sale.automobile.vin }</td>
                                        <td>{ sale.customer.phone_number }</td>
                                        <td>{ sale.price }</td>
                                    </tr>
                                );
                                })}
                            </tbody>
                        </table>
                </div>
        </div>
    )
}

export default SalesPersonHistory
