import React, { useState } from 'react';

function TechnicianForm() {

    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeID] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { first_name, last_name, employee_id };

        const technicianURL = 'http://localhost:8080/api/technicians/'
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(technicianURL, fetchOptions);
        if (response.ok) {
            const newTechnician = await response.json();
            setFirstName('');
            setLastName('');
            setEmployeeID('');

            window.location.reload();
        }
    }

    const handleChangeFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const handleChangeLastName = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const handleChangeEmployeeID = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="add-technician-form">
                        <h1 className="card-title">Add a Technician</h1>
                        <div className="col">
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeFirstName} required placeholder="First Name" type="text" id="first_name" name="first_name" className="form-control" />
                                    <label htmlFor="first_name">First Name</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeLastName} required placeholder="Last Name" type="text" id="last_name" name="last_name" className="form-control" />
                                    <label htmlFor="last_name">Last Name</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeEmployeeID} required placeholder="Employee ID" type="text" id="employee_id" name="employee_id" className="form-control" />
                                    <label htmlFor="employee_id">Employee ID</label>
                                </div>
                            </div>
                        </div>
                        <button className="btn btn-lg btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
