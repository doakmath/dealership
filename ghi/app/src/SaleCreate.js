import { useState, useEffect } from 'react'


function SaleCreate() {

    const [salespeople, setSalespeople] = useState([])
    const [salesperson, setSalesperson] = useState(0)
    const [customers, setCustomers] = useState([])
    const [customer, setCustomer] = useState()
    const [automobiles, setAutomobiles] = useState([])
    const [automobile, setAutomobile] = useState()
    const [price, setPrice] = useState(0)

    const handleSalespersonChange = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value
        setAutomobile(value)
    }

    const handlePriceChange = (event) => {
        const value = event.target.value
        setPrice(value)
    }

    const fetchAutomobilesData = async () => {
        const url = "http://localhost:8100/api/automobiles/"
        const response = await fetch(url)
        try {
            if (response.ok) {
            const data = await response.json()
            setAutomobiles(data.autos)
            }
        } catch (error) {
            console.error("Error retrieving SALESPEOPLE: fetchData", error)
        }
    }

    useEffect(() => {
        fetchAutomobilesData()
    }, []);

    const fetchSalespeopleData = async () => {
        const url = "http://localhost:8090/api/salespeople/"
        const response = await fetch(url)
        try {
            if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salespeople)
            }
        } catch (error) {
            console.error("Error retrieving SALESPEOPLE: fetchData", error)
        }
    }

    useEffect(() => {
        fetchSalespeopleData()
    }, []);

    const fetchCustomersData = async () => {
        const url = "http://localhost:8090/api/customers/"
        const response = await fetch(url)
        try {
            if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
            }
        } catch (error) {
            console.error("Error retrieving CUSTOMERS: fetchData", error)
        }
    }

    useEffect(() => {
        fetchCustomersData()
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.automobile = automobile
        data.salesperson = salesperson
        data.customer = customer
        data.price = price
        const automobileUrl = `http://localhost:8100/api/automobiles/${automobile}/`
        const url = "http://localhost:8090/api/sales/"
        const autoData = {"sold": true}
        const fetchAutoConfig = {
            method: "PUT",
            body: JSON.stringify(autoData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const responseAuto = await fetch(automobileUrl, fetchAutoConfig)
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newSale = await response.json()
            console.log("newSale", newSale)
            setAutomobiles("")
            setCustomers("")
            setSalespeople("")
            setPrice("")
            created()
            window.location.reload()
        }
    }

    function created() {
        return(
            alert("You did it!  You created a sale!")
        )
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Sale</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="mb-3">
                            <select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
                            <option value="">Choose a Salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                                )})}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={customer} onChange={handleCustomerChange} required id="customers" name="customers" className="form-select">
                            <option value="">Choose a Customer</option>
                            {customers.map(customer => {
                                return (
                                <option key={customer.id} value={customer.phone_number}>{customer.first_name} {customer.last_name}</option>
                                )})}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={automobile} onChange={handleAutomobileChange} required id="automobiles" name="automobiles" className="form-select">
                            <option value="">Choose an Automobile by VIN</option>
                            {automobiles.filter(automobile => automobile.sold === false ).map(automobile => {
                                return (
                                <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                                )})}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={price} onChange={handlePriceChange} placeholder="price" type="text" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button onClick={handleSubmit} className="btn btn-primary">Create</button>
                        <div>
                        </div>
                        <div className="alert alert-secondary" role="alert">
                            <div>
                                Employee ID: {salesperson}
                            </div>
                            <div>
                                Customer Phone Number: {customer}
                            </div>
                            <div>
                                VIN: {automobile}
                            </div>
                            <div>
                                Price: ${price}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SaleCreate
